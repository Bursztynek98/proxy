
import java.util.ArrayList;
import java.util.List;

interface Bank {
    public int GetMoney(int id);
    public void AddMoney(int id,int count);
    public void AddAccount();
}

class BankImp implements Bank {

    private List<Integer> list = new ArrayList<Integer>();

    public BankImp(){
        BankInit();
    }

    public int GetMoney(int id) {
        return list.get(id);
    }
    public void AddMoney(int id,int count) {
        list.set(id,list.get(id)+count);
        System.out.println("Add "+count+" to "+id );
    }
    public void AddAccount(){
        list.add(0);
    }

    private void BankInit(){
        System.out.println("Load Bank");
    }
}

class BankProxy implements Bank{
    private static Bank obj;

    @Override
    public int GetMoney(int id){
        if (obj==null){
            obj = new BankImp();
        }
        return obj.GetMoney(id);
    }
    @Override
    public void AddMoney(int id,int count){
        if (obj==null){
            obj = new BankImp();
        }
        obj.AddMoney(id,count);
    }
    @Override
    public void AddAccount(){
        if (obj==null){
            obj = new BankImp();
        }
        obj.AddAccount();
    }
}






public class Main {
    public static void main(String[] args) {
        Bank bank = new BankProxy();
        System.out.println("Hello");
        bank.AddAccount();
        bank.AddAccount();
        bank.AddAccount();

        System.out.println(bank.GetMoney(1));

        bank.AddMoney(1,244);
        System.out.println("T1");

        Bank bank2 = new BankProxy();
        System.out.println("T2");
        System.out.println(bank2.GetMoney(1));
    }
}
